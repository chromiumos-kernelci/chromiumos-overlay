# Copyright 2023 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: cros-kernel2.eclass
# @MAINTAINER:
# The ChromiumOS Authors <chromium-os-dev@chromium.org>
# @BUGREPORTS:
# Please report bugs via
# https://issuetracker.google.com/issues/new?component=167278
# @VCSURL: https://chromium.googlesource.com/chromiumos/overlays/chromiumos-overlay/+/HEAD/eclass/@ECLASS@
# @BLURB: helper eclass for building ChromiumOS kernel packages
# @DESCRIPTION:
# Common code shared between kernel package ebuilds.

if [[ -z "${_ECLASS_CROS_KERNEL}" ]]; then
_ECLASS_CROS_KERNEL=1

inherit cros-kernel2

# TODO(b/296938456): Remove these aliases after all ebuilds are switched from
# cros-kernel2.eclass and its contents are moved here.
cros-kernel_pkg_setup() {
	cros-kernel2_pkg_setup
}
cros-kernel_src_unpack() {
	cros-kernel2_src_unpack
}
cros-kernel_src_prepare() {
	cros-kernel2_src_prepare
}
cros-kernel_src_configure() {
	cros-kernel2_src_configure
}
cros-kernel_src_compile() {
	cros-kernel2_src_compile
}
cros-kernel_src_install() {
	cros-kernel2_src_install "$@"
}

fi  # _ECLASS_CROS_KERNEL

EXPORT_FUNCTIONS pkg_setup src_unpack src_prepare src_configure src_compile src_install
