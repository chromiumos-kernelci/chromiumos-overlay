# Copyright 2019 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

CROS_WORKON_COMMIT="1f4189559e609bbd21a8032a9a0f0e9884659ff3"
CROS_WORKON_TREE=("f91b6afd5f2ae04ee9a2c19109a3a4a36f7659e6" "d5188fec8e14c2a9a2c16c9645dc83908534149c" "b436b2188c971c497008cf6cfcc56b0b718d924c" "13a1732d79ff7a78aec895914901f7207cb5770c" "30bfaefc2eed36d56ffbbb2151a90e6ade095d6a" "f12a2f998297133c0e97f61e1fa8f901e7f6852b" "6004a0e1699013ec8cf7e1061b6ed785e492f1a1" "224719ec7288d2e56e7e1ed4f916d24934babb25" "7e22014f07fcc3faa5fb98a17f81dc91b4ca47ba")
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_LOCALNAME="../platform2"
CROS_WORKON_SUBTREE=".gn camera/build camera/common camera/hal/ip camera/hal/usb camera/include camera/mojo chromeos-config common-mk"
CROS_WORKON_OUTOFTREE_BUILD="1"
CROS_WORKON_INCREMENTAL_BUILD="1"

PLATFORM_SUBDIR="camera/hal/ip"

inherit cros-camera cros-workon platform

DESCRIPTION="Chrome OS IP camera HAL v3."

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
RDEPEND="
	chromeos-base/chromeos-config-tools
	chromeos-base/cros-camera-android-deps
	chromeos-base/cros-camera-libs
	media-libs/libsync"

DEPEND="${RDEPEND}
	virtual/pkgconfig"

platform_pkg_test() {
	platform test_all
}
