# Copyright 2023 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
CROS_WORKON_COMMIT=("db0f4a65bb5d030a9410753b9f3e3381ef47d79c" "e85eb85fd06cc9e72f57e889d954a001f86d61d3")
CROS_WORKON_TREE=("b19a06e06c2031617e28402f02efb418a910dcb9" "57f43fc8a340694898345c87295a6822915490a0")
CROS_WORKON_PROJECT=(
	"chromiumos/platform/dev-util"
	"chromiumos/config"
)

CROS_WORKON_LOCALNAME=(
	"../platform/dev"
	"../config"
)

CROS_WORKON_SUBTREE=(
	"src/chromiumos/test/check"
	"python"
)

CROS_WORKON_DESTDIR=(
	"${S}"
	"${S}/config"
)

inherit cros-go cros-workon

DESCRIPTION="Utility to check if a DUT is ready to be used for testing"
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform/dev-util/+/HEAD/src/chromiumos/test/check"

LICENSE="BSD-Google"
KEYWORDS="*"
IUSE=""

CROS_GO_VERSION="${PF}"

CROS_GO_BINARIES=(
	"chromiumos/test/check/cmd/cros_test_ready"
)

CROS_GO_TEST=(
	"chromiumos/test/check/cmd/cros_test_ready/..."
)

CROS_GO_VET=(
	"${CROS_GO_TEST[@]}"
)

DEPEND="
	chromeos-base/cros-config-api
	chromeos-base/autotest-client
	chromeos-base/tast-local-tests-cros
	dev-go/protobuf-legacy-api
	dev-go/subcommands
"

src_prepare() {
	export CGO_ENABLED=0
	export GOPIE=0

	default
}

src_install() {
	default
	cros-go_src_install
	# Set the path for the configure file generator.
	local generator="${S}/src/chromiumos/test/check/python/cros_test_ready_config_generator.py"
	local path="${WORKDIR}/cros_test_ready_config.jsonpb"

	export PYTHONDONTWRITEBYTECODE=1
	"${generator}" -src_root "${ROOT}" -output_file="${path}" || die

	insinto /etc
	doins "${path}"
}
