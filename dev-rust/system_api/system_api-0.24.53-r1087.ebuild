# Copyright 2019 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_COMMIT="ea8fd0019b75109dea3fdf7992b0ff8aa3274086"
CROS_WORKON_TREE=("b8cfd0df66e51534cbf0a7643761d4fb8f576bce" "a0d8550678a1ed2a4ab62782049032a024bf40df" "686d40aba2d1e8ff61b6332372c5bedab1b1b693" "5fc98c479581f2e74f8be63b2063c6e9a261aff4" "89deb3f8b08c03b37a1d010a087c12aa51147163" "3795880a80f1e0b72d7853fb8d689abc2e9ad289" "f8b4f9a1b4064b4f96d7fd2f22e167523c608033" "63d3d2b8ce71b0a7966b3d9530e9b12633b744b0" "3a7df68f70c7d697449fd6a965342139eb4dce18" "89b258d4a30905f921b5d553f050a93b168e1a80" "fb7927f3a2a4ef162cfa0aa35968cbf14ed01be0" "2cb8078e3907b4b5ab01a746fb499a48918de614")
CROS_RUST_SUBDIR="system_api"

CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_LOCALNAME="../platform2"
CROS_WORKON_SUBTREE="${CROS_RUST_SUBDIR} authpolicy/dbus_bindings cryptohome/dbus_bindings debugd/dbus_bindings dlcservice/dbus_adaptors login_manager/dbus_bindings shill/dbus_bindings spaced/dbus_bindings power_manager/dbus_bindings printscanmgr/dbus_bindings vm_tools/dbus_bindings vtpm"

inherit cros-workon cros-rust

DESCRIPTION="Chrome OS system API D-Bus bindings for Rust."
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/system_api/"

LICENSE="BSD-Google"
SLOT="0/${PVR}"
KEYWORDS="*"

DEPEND="
	cros_host? ( dev-libs/protobuf:= )
	dev-rust/third-party-crates-src:=
	dev-rust/chromeos-dbus-bindings:=
	sys-apps/dbus:=
"
# (crbug.com/1182669): build-time only deps need to be in RDEPEND so they are pulled in when
# installing binpkgs since the full source tree is required to use the crate.
RDEPEND="${DEPEND}
	!chromeos-base/system_api-rust
"

BDEPEND="
	dev-libs/protobuf
	dev-rust/chromeos-dbus-bindings
"

src_install() {
	# We don't want the build.rs to get packaged with the crate. Otherwise
	# we will try and regenerate the bindings.
	rm build.rs || die "Cannot remove build.rs"

	cros-rust_src_install
}
