# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

EAPI="7"

CROS_WORKON_COMMIT="a5f74da15faab8d0fe924a38607692f29ae4b8bf"
CROS_WORKON_TREE=("7e22014f07fcc3faa5fb98a17f81dc91b4ca47ba" "cb585f887abc135c2eac6123c5df2c42f07d02f0" "a8740496c659ec718a3156870b765a911886eb0c" "f91b6afd5f2ae04ee9a2c19109a3a4a36f7659e6")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_DESTDIR="${S}/platform2"
CROS_WORKON_SUBTREE="common-mk bootsplash libec .gn"

PLATFORM_SUBDIR="bootsplash"

inherit cros-workon platform user

DESCRIPTION="Frecon-based boot splash service"
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/bootsplash"

LICENSE="BSD-Google"
SLOT="0/0"
KEYWORDS="*"

DEPEND="
	chromeos-base/bootstat:=
	chromeos-base/session_manager-client:=
	chromeos-base/system_api:=
	dev-libs/re2:=
"

RDEPEND="
	${DEPEND}
	sys-apps/frecon
"

pkg_preinst() {
	enewuser "bootsplash"
	enewgroup "bootsplash"
}

src_install() {
	platform_src_install

	dobin "${OUT}/bootsplash"
}

platform_pkg_test() {
	platform test_all
}
