# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

EAPI=7

CROS_WORKON_COMMIT="2b64f26a2c7326d217d55c4523b937b166f47d36"
CROS_WORKON_TREE=("e762f730ed7fab2eb01b102f6d87721b35f2c225" "7e22014f07fcc3faa5fb98a17f81dc91b4ca47ba" "351fb08db6b0e98a2c6de16681f05c1876359158" "a99495a599e084a7a1a880005e1097028659d164" "6b07f569680a972777d182f14b4ffa48eac094c0" "308e277ee3ac451d85553989b6b4e5ba6e0250e0" "6ad698aa48a3c7ff436d72914bfbd5d1121cd4d2" "f91b6afd5f2ae04ee9a2c19109a3a4a36f7659e6")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_OUTOFTREE_BUILD=1
CROS_WORKON_SUBTREE="attestation common-mk hwsec-test-utils libhwsec libhwsec-foundation tpm_manager trunks .gn"

PLATFORM_SUBDIR="hwsec-test-utils"

inherit cros-workon platform

DESCRIPTION="Hwsec-related test-only features. This package resides in test images only."
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/hwsec-test-utils/"

LICENSE="BSD-Google"
KEYWORDS="*"
IUSE="test tpm tpm_dynamic tpm2"
REQUIRED_USE="
	tpm_dynamic? ( tpm tpm2 )
	!tpm_dynamic? ( ?? ( tpm tpm2 ) )
"

RDEPEND="
	tpm? (
		app-crypt/trousers:=
	)
	chromeos-base/attestation:=
	chromeos-base/libhwsec:=
	chromeos-base/libhwsec-foundation:=
	chromeos-base/system_api:=
	tpm2? (
		chromeos-base/trunks:=
	)
	dev-libs/openssl:=
	dev-libs/protobuf:=
"

DEPEND="${RDEPEND}
	tpm2? (
		chromeos-base/trunks:=[test?]
	)
"

platform_pkg_test() {
	platform test_all
}
