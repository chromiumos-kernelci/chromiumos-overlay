# Copyright 2023 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
CROS_WORKON_COMMIT="1f1ac8c4e26e4036450f2fa4099f7c132588f7b3"
CROS_WORKON_TREE=("7e22014f07fcc3faa5fb98a17f81dc91b4ca47ba" "bba4b363401b90391c613a7e48c94aceec299aa8" "61d04cb885f0012618667007319ca3f66758d381" "f91b6afd5f2ae04ee9a2c19109a3a4a36f7659e6" "1f8e0d50ae6bb0fb892d2776e8135efc59e5108a")
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_DESTDIR="${S}/platform2"
CROS_WORKON_SUBTREE="common-mk diagnostics flex_hwis .gn metrics"

PLATFORM_SUBDIR="flex_hwis"

inherit cros-workon platform

DESCRIPTION="Utility to collect/send Hardware Information for ChromeOS Flex"
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/flex_hwis"

LICENSE="BSD-Google"
KEYWORDS="*"

RDEPEND="
	acct-group/flex_hwis
	acct-user/flex_hwis
	chromeos-base/diagnostics:=
"

platform_pkg_test() {
	platform test_all
}
