# Copyright 2016 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_COMMIT="c46926056da0fc655aeea9a0efb8b8adfbc0fed7"
CROS_WORKON_TREE=("86e242fd78068b7f35e9aeee3cfc4708288863bf" "ebc26713c508efe528db546465563e5506f6a3d4" "a2c4e8bfad2f8cb966be372e632ff6d9c21bf4cc" "b9a99cafa5df09d07b7c4d83a316baf8acf6ae80" "13353fa49b28277922104769ac9ea714afe4ffee" "4a8a3d2135b93784093c7f4c5d2a54b921930145" "ddc198a1f3d5ce296d267f981ca0b0bb3b340d5c")
CROS_WORKON_PROJECT="apps/libapps"
CROS_WORKON_LOCALNAME="third_party/libapps"
CROS_WORKON_SUBTREE="libdot hterm nassh ssh_client terminal wasi-js-bindings wassh"

inherit cros-workon

DESCRIPTION="The Chromium OS Shell extension (the HTML/JS rendering part)"
HOMEPAGE="https://chromium.googlesource.com/apps/libapps/+/master/nassh/doc/chromeos-crosh.md"
# These are kept in sync with libdot.py settings.
FONTS_HASHES=(
	# Current one.
	d6dc5eaf459abd058cd3aef1e25963fde893f9d87f5f55f340431697ce4b3506
	# Next one.
)
NODE_HASHES=(
	# Current one.
	16.13.0/ab9544e24e752d3d17f335fb7b2055062e582d11
	# Next one.
)
NPM_HASHES=(
	# Current one.
	d71ef656041c1a9560fdf09c0b7c180531a5a1a92d6bd588aee0b2370cabfd39
	# Next one.
	473756c69f6978a0b9ebb636e20c6afc0a05614e9bf728dec3b6303d74799690
)
PLUGIN_VERSIONS=(
	# Current one.
	0.54
	# Next one.
	0.58
)
SRC_URI="
	$(printf 'https://storage.googleapis.com/chromium-nodejs/%s ' "${NODE_HASHES[@]}")
	$(printf 'https://storage.googleapis.com/chromeos-localmirror/secureshell/distfiles/fonts-%s.tar.xz ' \
		"${FONTS_HASHES[@]}")
	$(printf 'https://storage.googleapis.com/chromeos-localmirror/secureshell/distfiles/node_modules-%s.tar.xz ' \
		"${NPM_HASHES[@]}")
	$(printf 'https://storage.googleapis.com/chromeos-localmirror/secureshell/releases/%s.tar.xz ' \
		"${PLUGIN_VERSIONS[@]}")
"

# The archives above live on Google maintained sites.
RESTRICT="mirror"

LICENSE="BSD-Google"
SLOT="0/0"
KEYWORDS="*"
IUSE=""

RDEPEND="!<chromeos-base/common-assets-0.0.2"

BDEPEND="
	app-arch/unzip
	app-arch/zip
	sys-devel/gcc
"

e() {
	echo "$@"
	"$@" || die
}

src_compile() {
	export VCSID="${CROS_WORKON_COMMIT:-${PF}}"
	e ./nassh/bin/mkdist --crosh-only
}

src_install() {
	local dir="/usr/share/chromeos-assets/crosh_builtin"
	dodir "${dir}"
	unzip -d "${D}${dir}" nassh/dist/crosh.zip || die
	local pnacl="${D}${dir}/plugin/pnacl"
	if ! use arm && ! use arm64; then
		rm "${pnacl}/ssh_client_nl_arm.nexe"* || die
	fi
	if ! use x86 ; then
		rm "${pnacl}/ssh_client_nl_x86_32.nexe"* || die
	fi
	if ! use amd64 ; then
		rm "${pnacl}/ssh_client_nl_x86_64.nexe"* || die
	fi
}
