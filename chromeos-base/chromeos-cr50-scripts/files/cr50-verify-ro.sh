#!/bin/bash
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# This script provides user interface to the 'open box RMA' procedure which
# verifies integrity of the RO areas of the RMAed device's AP and EC firmware.
#
# The script is run on a secure Chrome OS device which is connected with SuzyQ
# cable to the RMAed device (also referred to as "device under test" a.k.a.
# DUT).
#
# The script checks the Cr50 version run on the DUT and updates it if
# necessary. After that this script invokes gsctool to perform actual RO
# verification.

/usr/share/cros/hwsec-utils/cr50_verify_ro "$@"
