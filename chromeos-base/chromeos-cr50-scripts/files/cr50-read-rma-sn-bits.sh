#!/bin/sh
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# This script reads S/N vNVRAM data from cr50 as a part of the factory process.
# The data is printed as vvvvvv:rr:ss..ss, where
#   vvvvvv is the hex representation of the 3 version bytes,
#   rr is the hex representation of the RMA status byte,
#   ss..SS is the hex representation of SN Bits (12 bytes).

/usr/share/cros/hwsec-utils/cr50_read_rma_sn_bits "$@"
