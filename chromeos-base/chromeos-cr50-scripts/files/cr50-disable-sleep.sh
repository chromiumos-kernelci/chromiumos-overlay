#!/bin/bash
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# This script montiors dbus for suspend events and then sends a command
# to the cr50 to disable deep sleep during suspend.
# The disable deep sleep flag is transiant, so it must be resent before
# each suspend.

/usr/share/cros/hwsec-utils/cr50_disable_sleep "$@"
