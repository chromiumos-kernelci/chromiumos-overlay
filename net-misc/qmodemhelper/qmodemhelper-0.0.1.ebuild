# Copyright 2023 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit cmake

DESCRIPTION="EM060 firmware flashing tool"
HOMEPAGE="https://github.com/quectel-official/QModemHelper"
GIT_SHA1="4e2793a8391a08a6b7e86b9c8666b841f55f721f"
SRC_URI="https://github.com/quectel-official/QModemHelper/archive/${GIT_SHA1}.tar.gz -> QModemHelper-${GIT_SHA1}.tar.gz"


LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="*"
IUSE=""

RDEPEND="dev-libs/libxml2:=
	net-misc/modemmanager-next:="

DEPEND="${RDEPEND}"

S="${WORKDIR}/QModemHelper-${GIT_SHA1}"
